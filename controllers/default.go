package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/config"
	"github.com/jlaffaye/ftp"
	"time"
	"encoding/json"
	"os"
	"io"
	"log"
	"strings"
	"io/ioutil"
	"strconv"
)

type MainController struct {
	beego.Controller
}

type TemplateItem struct {
	Code string
	Label string
	Value string
	Type string
}

func (c *MainController) Prepare() {
	c.Data["HeadStyles"] = []string{
		"../static/css/base.css",
		"../static/css/bootstrap.css",
	}

	c.Data["HeadScripts"] = []string{
		"/static/js/jquery-2.1.4.js",
		"/static/js/bootstrap.js",
		"/static/js/base.js",
		"/static/js/bootbox.js",
	}
}

func (c *MainController) Get() {
	file, _ := os.Open("conf/config.json")

	templatesInputData := []TemplateItem{}
	templatesTextAreaData := []TemplateItem{}
	decoder := json.NewDecoder(file)
	for {
		templateItem := new(TemplateItem)
		if err := decoder.Decode(&templateItem); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		if templateItem.Type == "input" {
			templatesInputData = append(templatesInputData, *templateItem)
		} else {
			templatesTextAreaData = append(templatesTextAreaData, *templateItem)
		}

	}
	c.Data["ContentInput"] = templatesInputData
	c.Data["ContentTextArea"] = templatesTextAreaData
	c.TplNames = "index.tpl"
}

func (c *MainController) GenerateCommand() {
	currentTime := int32(time.Now().Unix())

	requestItems := c.Input()
	data := make(map[string]string);
	for key, value := range requestItems {
		data[key] = strings.Join(value, "")
	}
	jsonString, _ :=json.Marshal(data)
	jsonData := []byte(string(jsonString))
	jsonFilename := strings.Join([]string{"report_", strconv.Itoa(int(currentTime)), ".json"}, "")
	ioutil.WriteFile(strings.Join([]string{"data/temp/", jsonFilename}, ""), jsonData, 0644)

	ftpconf, _ := config.NewConfig("ini", "conf/ftp.conf")

	ftpUrl := strings.Join([]string{ftpconf.String("FtpHost"),":",ftpconf.String("FtpPort")}, "")
	ftp, _ := ftp.Connect(ftpUrl)

	ftp.Login(ftpconf.String("FtpUser"), ftpconf.String("FtpPass"))

	jsonReader, _ := os.Open(strings.Join([]string{"data/temp/", jsonFilename}, ""))
	ftp.Stor(strings.Join([]string{ftpconf.String("FtpDataUploadDir"), "/", jsonFilename}, ""), jsonReader)

	templateReader, _ := os.Open("data/template.docx")
	ftp.Stor(strings.Join([]string{ftpconf.String("FtpTemplateUploadDir"), "/", "report_", strconv.Itoa(int(currentTime)), ".docx"}, ""), templateReader)

	ftp.Quit()

	result := make(map[string]string, 2)
	result["success"] = "1"
	result["name"] = strconv.Itoa(int(currentTime))
	c.Data["json"] = result
	c.ServeJson()
}

func (c *MainController) CheckCommand() {
	fileName := c.Input().Get("name")
	fullFileName := strings.Join([]string{"report_", fileName, ".docx"}, "")

	ftpconf, _ := config.NewConfig("ini", "conf/ftp.conf")
	ftpUrl := strings.Join([]string{ftpconf.String("FtpHost"),":",ftpconf.String("FtpPort")}, "")
	ftp, _ := ftp.Connect(ftpUrl)
	ftp.Login(ftpconf.String("FtpUser"), ftpconf.String("FtpPass"))

	files, _ := ftp.NameList(ftpconf.String("FtpDownloadDir"))

	result := make(map[string]string, 1)
	for i := range files {
		if strings.Contains(files[i], fullFileName) {
			result["success"] = "1"
			c.Data["json"] = result
			c.ServeJson()
			ftp.Quit()
			return;
		}
	}

	files, _ = ftp.NameList(ftpconf.String("FtpErrorDir"))

	for i := range files {
		if strings.Contains(files[i], fullFileName) {
			result["success"] = "-1"
			c.Data["json"] = result
			c.ServeJson()
			ftp.Quit()
			return;
		}
	}

	result["success"] = "0"
	c.Data["json"] = result
	c.ServeJson()
	ftp.Quit()
}

func (c *MainController) GetCommand() {
	fileName := c.Input().Get("name")
	fullFileName := strings.Join([]string{"report_", fileName, ".docx"}, "")

	ftpconf, _ := config.NewConfig("ini", "conf/ftp.conf")
	ftpUrl := strings.Join([]string{ftpconf.String("FtpHost"),":",ftpconf.String("FtpPort")}, "")
	ftp, _ := ftp.Connect(ftpUrl)
	ftp.Login(ftpconf.String("FtpUser"), ftpconf.String("FtpPass"))

	ftpFileName := strings.Join([]string{ftpconf.String("FtpDownloadDir"), "/", fullFileName}, "")
	r, _ := ftp.Retr(ftpFileName);
	fileData, _ := ioutil.ReadAll(r);

	resultFilename := "data/result/" + fullFileName
	ioutil.WriteFile(resultFilename, fileData, 777);


	c.Ctx.Output.Header("Content-Type", "application/download")
	c.Ctx.Output.Header("Content-Disposition", strings.Join([]string{"attachment; filename=", fullFileName}, ""))
	c.Ctx.Output.Body(fileData)
}

