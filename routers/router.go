package routers

import (
	"reporter/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
	beego.Router("/generate", &controllers.MainController{}, "post:GenerateCommand")
	beego.Router("/check", &controllers.MainController{}, "post:CheckCommand")
	beego.Router("/get", &controllers.MainController{}, "get:GetCommand")
}
