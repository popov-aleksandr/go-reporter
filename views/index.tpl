<!DOCTYPE html>

<html>
<head>
  <title>Создание отчета</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  {{range .HeadStyles}}
              <link rel="stylesheet" type="text/css" href="{{.}}">
          {{end}}
  {{range .HeadScripts}}
    <script src="{{.}}"></script>
  {{end}}
</head>

<body>
<div class="container">
      <div class="page-header">
        <h3>Заполнение шаблона документа на обновление боя</h3>
      </div>
      <form method="post">
      <table class="table table-striped">
      <tr class="success">
              <th class="col-md-3">Поле для заполнения:</th>
              <th class="col-md-3">Текст:</th>
      <tr>
      {{range .ContentInput}}
        <tr id="{{.Code}}">
            <td class="col-md-3">{{.Label}}</td>
            <td class="col-md-6"><input name="{{.Code}}" value="{{.Value}}" type="text" style=" width: 100%;"></td>
        </tr>
      {{end}}
      <br>
      {{range .ContentTextArea}}
                    <tr id="{{.Code}}">
                        <td class="col-md-3">{{.Label}}</td>
                        <td class="col-md-6"><textarea name="{{.Code}}" value="{{.Value}}" type="text" style=" width: 100%;"></textarea></td>
                    </tr>
      {{end}}
      </table>
      <button type="submit" class="btn btn-primary">Сформировать</button>
      </form>
  </div>
</body>
</html>
