package main

import (
	_ "reporter/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

