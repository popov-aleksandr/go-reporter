function checkState($name) {
    var data = {name: $name};
    $.post('/check', data, function (json) {
        if (json.success != 1) {
            if (json.success == -1) {
                var $message = "<p>При формировании отчета произошла ошибка...</p><br>";
                $('.generate .bootbox-body').html($message);
            } else {
                setTimeout(function() {
                    checkState($name);
                }, 1000);
            }
        } else {
            var $message = "<p>Отчет сформирован</p><br>";
            $message = $message + '<a href=/get?name=' + $name + '>Скачать сформированный отчет</a>';
            $('.generate .bootbox-body').html($message);
        }
    }, 'json');
}

function generateProtocol() {
    var params = {
        className: "generate",
        message: 'Отправка данных...',
        title: "Формирование отчета...",
        buttons: {
            primary: {
                label: "Закрыть",
                className: "btn-primary",
                callback: function() {
                    bootbox.hideAll();
                }
            }
        }
    };
    bootbox.dialog(params);
    var formData = $('form').serializeArray();
    $.post('/generate', formData, function (json) {
                if (json.success != -1) {
                    console.log(json.success != -1);
                } else {

                }
                }, 'json');

    $.post('/generate', formData, function (json) {
        if (json.name) {
            $('.generate .bootbox-body').html("<p>Идет формирование отчета...</p>");
            setTimeout(function() {
                checkState(json.name);
            }, 1000);
        } else {
            var $message = "<p>При формировании отчета произошла ошибка...</p><br>";
            $('.generate .bootbox-body').html($message);
        }
    }, 'json');
}

$(function(){
    $("button").on('click', function (event) {
            event.preventDefault();

            generateProtocol();
        });
});